#!/usr/bin/env bash

#    macvlan_bypass.sh - Create network namespaces with bridged macvlans
#    Copyright (C) 2018  Joshua Mallad

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

function genlocal6() {
	addr="fe80:"
	for ((i=0; i<4; i++)); do
		addr="${addr}$(printf %x%x: $(($RANDOM % 255)) $(($RANDOM % 255)))"
	done
	echo $addr:/64
}

syntax() {
    IFS='' read -r -d '' output << EOF
Syntax: $0 <args...>

The following values MUST be defined, in a config file or on the command-line:
  --primary <name>   | Primary network interface
Optional arguments:
  --config <path>    | Path to config file - the following command-line
                     | arguments will override any values found inside the file
  -4                 | IPv4-only mode - disable IPv6 on all interfaces
                     | in the new namespace
  --bridge-mode      | Create a new bridge device in the namespace and 
                     | enslave the secondary macvlan device
  --netns <name>     | Name of new network namespace (default random UUID)
  --bridge <name>    | Name of bridge interface (default br0)
  --secondary <name> | Name of secondary interface (default eth0)
Addressing:
  --ipv4-mode <dhcp|static|none>
    Select how to configure IPv4 addresses. DHCP is default and requires
    a DHCPv4 server to be accessible to the primary interface. Static
    requires that --ipv4 and --gateway are defined. None doesn't configure
    any IPv4 addresses except localhost (127.0.0.1).
  --ipv6-mode <slaac|dhcp|static|none>
    Select how to configure IPv6 addresses. SLAAC auto-generation is default.
    DHCP requires a DHCPv6 server to be accessible to the primary interface.
    Static requires that --ipv6 and --gateway6 are defined. None doesn't
    configure any IPv6 addresses except localhost (::1).
  --ipv4  <address/mask>          | IPv4 address with netmask
  --gateway  <address>            | IPv4 default gateway
  --ipv6 <address/mask>           | IPv6 address with netmask
  --gateway6 <address>            | IPv6 default gateway
DHCP options:
  --dhcp-client <dhclient|dhcpcd> | Which dhcp client to use 
  --dhcp-config <path>            | Path to config file for DHCP client
DNS options:
  --nameservers <a,b...> | Comma-separated list of DNS resolvers to use in the
                         | namespace - defaults to Google Public DNS
Summary:
  This script is used to create an isolated network namespace
  with a new macvlan interface bridged to an existing primary interface.
  The secondary interface is configured with an alternate set of
  addresses and routes.
EOF
    echo -en "$output"
    exit 255
}

if [ -z "$1" ]; then
    syntax
fi

arg="$1"
while [ "${arg:0:1}" == "-" ]; do
    arg="${arg:1:$((${#arg}-1))}"
	if [ "$arg" == "h" ] || [ "$arg" == "-help" ]; then
		syntax
	fi
	case "$arg" in
		"-destroy")
			status=0
			shift
			while read pid; do
				if [ -z "$pid" ]; then
					break;
				fi
				kill -9 "$pid"
				((status+="$?"));
			done <<< "$(ip netns pids $1)"
			ip netns del "$1"
			((status+="$?"));
			rm -rf "/etc/netns/$1"
			((status+="$?"));
			if [ "$status" -ne "0" ]; then
				echo "[error] $0: main: errors while destroying namespace"
				exit 1
			else
				echo "[info] $0: main: namespace $1 destroyed"
				exit 0
			fi
			;;
		"4")
			export ipv4_only=y
			export ipv4_mode=none
			;;
		"-bridge-mode")
			export bridge_mode="true"
			;;
		"-bridge")
			shift && export bridge="$1"
			;;
		"-config")
			shift && export config="$1"
			;;
		"-primary")
			shift && export primary="$1"
			;;
		"-secondary")
			shift && export secondary="$1"
			;;
		"-ipv4-mode")
			shift && export ipv4_mode="$1"
			;;
		"-ipv6-mode")
			shift && export ipv6_mode="$1"
			;;
		"-dhcp-client")
			shift && export dhcp_client="$1"
			;;
		"-dhcp-config")
			shift && export dhcp_config="$1"
			;;
		"-ipv4")
			shift && export ipv4="$1"
			;;
		"-gateway")
			shift && export gateway="$1"
			;;
		"-ipv6")
			shift && export ipv6="$1"
			;;
		"-gateway6")
			shift && export gateway6="$1"
			;;
		"-netns")
			shift && export netns="$1"
			;;
		"-nameservers")
			shift && export nameservers="$1"
			;;
		*)
			echo "[fatal] $0: Unrecognized argument: $1"
			syntax
			;;
	esac
    shift
    arg="$1"
done

if [ ! -z "$config" ]; then
	while read line; do
		if [ -z "$line" ]; then
			break
		fi
		line="${line// /}"
		key="${line%%=*}"
		key="${key//-/_}"
		if [ ! -z "${!key}" ]; then
			echo "[warn] $0: main: skipping config key \`$key' - already defined"
		else
			value="${line##*=}"
			export ${key}="$value"
		fi
	done < "$config"
fi

if [ -z "$primary" ]; then
	echo "[fatal] $0: Primary interface not defined"
	syntax
fi

if [ -z "$netns" ]; then
	export netns="$(uuidgen)"
	echo "--- NAMESPACE ID ---"
	echo "$netns"
	echo "--- ------------ ---"
fi
if [ -z "$nameservers" ]; then
	export nameservers="8.8.8.8,8.8.4.4,2001:4860:4860::8888,2001:4860:4860::8844"
fi
if [ -z "$ipv4_mode" ]; then
	export ipv4_mode=dhcp
fi
if [ -z "$ipv6_mode" ]; then
	export ipv6_mode=slaac
fi

if [ "$ipv4_mode" == "static" ]; then
	if [ -z "$gateway" ]; then
		echo "[fatal] $0: IPv4 gateway not defined"
		syntax
	elif [ -z "$ipv4" ]; then
		echo "[fatal] $0: IPv4 address and network not defined"
		syntax
	fi
	export ipv4_addr="${ipv4%%/*}"
fi

if [ "$ipv6_mode" == "static" ]; then
	if [ -z "$gateway6" ]; then
		echo "[fatal] $0: IPv6 gateway not defined"
		syntax
	elif [ -z "$ipv6" ]; then
		echo "[fatal] $0: IPv6 address and network not defined"
		syntax
	fi
	export ipv6_addr="${ipv6%%/*}"
fi

if [ "$ipv4_mode" == "dhcp" ] || [ "$ipv6_mode" == "dhcp" ]; then
	if [ -z "$dhcp_client" ]; then
		which dhclient &>/dev/null
		if [ "$?" == "0" ]; then
			dhcp_client=dhclient
		else
			which dhcpcd &> /dev/null
			if [ "$?" == "0" ]; then
				dhcp_client=dhcpcd
			else
				echo "[fatal] $0: Couldn't locate dhclient or dhcpcd in PATH"
				exit 1
			fi
		fi
	elif [ "$dhcp_client" != "dhclient" ] && [ "$dhcp_client" != "dhcpcd" ]; then
		echo "[fatal] $0: dhcp-client must be dhclient or dhcpcd"
		exit 1
	fi
fi

if [ -z "$secondary" ]; then
	export secondary=eth0
fi
if [ "$bridge_mode" == "true" ] && [ -z "$bridge" ]; then
	export bridge=br0
fi
	
export nameservers="${nameservers//,/ }"

ip netns del "$netns" &>/dev/null

echo "[info] $0: main: configuring addresses and routes"
trap 'exit' ERR
set -x

ip netns add "$netns"

mkdir -p /etc/netns/"$netns"
if [ "$dhcp_client" == "dhclient" ]; then
	if [ ! -d "/etc/dhclient" ]; then
		mkdir /etc/dhclient
	fi
	mkdir -p /etc/netns/"$netns"/dhclient
	cp dhclient-script /etc/netns/"$netns"/dhclient
fi
>/etc/netns/"$netns"/resolv.conf
for ns in $nameservers; do 
    echo -e "nameserver $ns" >> /etc/netns/"$netns"/resolv.conf
done
if [ ! -z "$dhcp_config" ]; then
	cp "$dhcp_config" /etc/"$netns"/dhcp.conf
fi

#The idea behind changing the interface name later is to avoid possible race conditions
#from firing up the script in parallel with default or otherwise identical interface names
iftmp="eth$RANDOM$RANDOM"
ip l add link "$primary" "$iftmp" type macvlan mode private
ip l set "$iftmp" netns "$netns"
ip -n "$netns" l set "$iftmp" name "$secondary"
if [ "$bridge_mode" == "true" ]; then
	export target="$bridge"
	ip -n "$netns" l add "$target" type bridge
	ip -n "$netns" l set "$secondary" master "$target"
else
	export target="$secondary"
fi
if [ "$ipv4_only" == "true" ]; then
    ip netns exec "$netns" sysctl -w net.ipv6.conf.all.disable_ipv6=1
fi
if [ "$ipv6_mode" == "slaac" ]; then
	ip -n "$netns" l set "$target" addrgenmode random
	ip netns exec "$netns" sysctl -w net.ipv6.conf."$target".autoconf=1
	ip netns exec "$netns" sysctl -w net.ipv6.conf."$target".accept_ra=1
else
	ip -n "$netns" l set "$target" addrgenmode none
	ip netns exec "$netns" sysctl -w net.ipv6.conf."$target".autoconf=0
	ip netns exec "$netns" sysctl -w net.ipv6.conf."$target".accept_ra=0
fi
if [ "$ipv6_mode" == "dhcp" ] || [ "$ipv6_mode" == "static" ]; then
	ip -n "$netns" a add "$(genlocal6)" dev "$target"
fi

#Bring up interfaces
ip -n "$netns" l set lo up
ip -n "$netns" l set "$secondary" up
if [ "$bridge_mode" == "true" ]; then
	ip -n "$netns" l set "$target" up
fi

case "$ipv4_mode" in
	"dhcp")
		if [ "$dhcp_client" == "dhclient" ]; then
			if [ ! -z "$dhcp_config"]; then
				ip netns exec "$netns" dhclient -4 -cf /etc/dhcp.conf -sf /etc/dhclient/dhclient-script
			else
				ip netns exec "$netns" dhclient -4 -sf /etc/dhclient/dhclient-script "$target"
			fi
		elif [ "$dhcp_client" == "dhcpcd" ]; then
			if [ ! -z "$dhcp_config"]; then
				ip netns exec "$netns" dhcpcd -4 -f /etc/dhcp.conf "$target"
			else
				ip netns exec "$netns" dhcpcd -4 "$target"
			fi
		fi
		;;
	"static")
		ip -n "$netns" a add "$ipv4" dev "$target"
		ip -n "$netns" r add default via "$gateway"
		ip netns exec "$netns" arping -q -f -I "$target" -U -s "$ipv4_addr" "$gateway"
		ip netns exec "$netns" arping -q -f -I "$target" -s "$ipv4_addr" "$gateway"
		;;
	"none")
		;;
	*)
		echo "[fatal] $0: Invalid IPv4 mode specified"
		syntax
esac
case "$ipv6_mode" in
	"dhcp")
		if [ "$dhcp_client" == "dhclient" ]; then
			if [ ! -z "$dhcp_config"]; then
				ip netns exec "$netns" dhclient -6 -cf /etc/dhcp.conf -sf /etc/dhclient/dhclient-script
			else
				ip netns exec "$netns" dhclient -6 -sf /etc/dhclient/dhclient-script "$target"
			fi
		elif [ "$dhcp_client" == "dhcpcd" ]; then
			if [ ! -z "$dhcp_config"]; then
				ip netns exec "$netns" dhcpcd -6 -f /etc/dhcp.conf "$target"
			else
				ip netns exec "$netns" dhcpcd -6 "$target"
			fi
		fi
		;;
	"static")
		ip -n "$netns" a add "$ipv6" dev "$target"
		ip -n "$netns" -6 r add default via "$gateway6" dev "$target"
	;;
	"none")
	;;
	"slaac")
	;;
	*)
		echo "[fatal] $0: Invalid IPv6 mode specified"
		syntax
		;;
esac

if [ "$ipv4_only" == "true" ]; then
	cp ipv4gai.conf /etc/netns/"$netns"/gai.conf
fi
set +x
echo "[info] $0: main: completed successfully"
